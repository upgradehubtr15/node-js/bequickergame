var express = require('express');
var app     = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const usuarios = [];
let ronda = 1;
const max_rondas = 8;
let challenge = {
    board : ["blue Cuadrado","green Circulo","red Triangulo","green Cuadrado","red Circulo","blue Triangulo","red Cuadrado","blue Circulo","green Triangulo",],
    correctCombination : []
}
function createCorrectAnswer () {
    challenge.correctCombination = [];
    var times = 6;
    for(var i=0; i < times; i++){
        challenge.correctCombination.push(challenge.board[Math.floor(Math.random() * challenge.board.length)])
    }
}
createCorrectAnswer();

function searchUser(user, UsersArray){
    for (var i=0; i < UsersArray.length; i++) {
        if (UsersArray[i].usuario === user) {
            return UsersArray[i];
        }
    }
}

app.use(express.static(__dirname + '/../public'));

io.on('connection', function(socket){
    let user= "User"+ new Date().getTime();
    socket.userName = user;
    usuarios.push({
        usuario: user,
        puntuacion: 0
    })
    console.log('a user connected');
    io.emit('users',  usuarios );
    socket.emit('singleUser', user);
    socket.on('start', function start() {
        io.emit('challenge', challenge);
        console.log("Empezamos");
    });
    socket.on('proposeSolution', function solution(answer) {
        const checkCorrectAnswer = (correctCombination, propoSolucion) => {
            for(let i = 0; i < propoSolucion.length; i++) {
                if (propoSolucion[i] !== correctCombination[i]){
                    return false
                }
            }
            return true
        }
        if (checkCorrectAnswer(challenge.correctCombination, answer)) {
            createCorrectAnswer();
            winnerUser = searchUser(this.userName, usuarios)
            winnerUser.puntuacion +=1;
            ronda +=1;
            if (ronda < max_rondas) {
                io.emit('challenge', challenge);
                io.emit('users',  usuarios );
                console.log("correcto");
            } else {
                io.emit('users',  usuarios );
                io.emit("winner", winnerUser);
            }
        } else {
            console.log("respuesta incorrecta")
        }
    });

    socket.on('restart', function restart() {
        ronda = 0;
        for (var i=0; i < usuarios.length; i++) {
            usuarios[i].puntuacion = 0
        }
        io.emit('challenge', challenge);
        io.emit('users',  usuarios );
    });

});

http.listen(3000, function(){
    console.log('App listening on *:3000');
});
  
